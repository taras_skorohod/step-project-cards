


class Modal {
    constructor() {
        this.modal = document.createElement('div');
        this.modalBody = document.createElement('div');
        this.modalContent = document.createElement('div');
        this.modalCloser = document.createElement('a');
    }

    insert(...elements) {
        return this.modalContent.append(...elements);
    }

    create() {
        this.modal.classList.add('modal', 'modal--open');
        this.modalBody.className = 'modal__body';
        this.modalContent.className = 'modal__content';
        this.modalCloser.className = 'modal__close';
        this.modalCloser.setAttribute('href', '#');
        this.modalCloser.textContent = String.fromCharCode(0x2716);
        this.modalCloser.addEventListener('click', (e) => {
            e.preventDefault();
            if(this.modal.classList.contains('modal--open')) {
                this.modal.classList.remove('modal--open');
                this.modal.remove();
            }
        })

        this.modalContent.append(this.modalCloser);
        this.modalBody.append(this.modalContent);
        this.modal.append(this.modalBody);
        document.body.append(this.modal);
        return this.modal;
    }


    close() {
        if(this.modal.classList.contains('modal--open')) {
            this.modal.classList.remove('modal--open');
            this.modal.remove();
        }
    }

    open() {
        if(!this.modal.classList.contains('modal--open')) {
            this.modal.classList.add('modal--open');
        }
    }

    remove() {
        this.modal.remove();
    }

    title(titleText = '', initClass = '') {
        const title = document.createElement('p');
        title.textContent = titleText;
        title.classList.add('modal-title', initClass);
        this.modalContent.prepend(title);
    }
}

class Form {
    constructor() {
        this.form = document.createElement('form');
    }

    insert(...elements) {
        this.form.append(...elements);
    }

    create() {
        this.form.classList.add('form');
        return this.form;
    }
}

class Input {
    constructor() {
        this.input = document.createElement('input');
    }

    create() {
        return this.input;
    }

    baseAttr(type = 'text', id = '', value = '', placeholder = '', required = '', name = '') {
        this.input.setAttribute('type', type);
        if(id) this.input.setAttribute('id', id);
        if(value) this.input.setAttribute('value', value);
        if(placeholder) this.input.setAttribute('placeholder', placeholder);
        if(required) this.input.setAttribute('required', required);
        if(name) this.input.setAttribute('name', name);
    }

    error() {
        const errorSpan = document.createElement("span");
        errorSpan.classList.add('errSpan');
        errorSpan.textContent = 'Неверно заполнено поле!';
        if(!this.input.value.trim()) {
            if(this.input.nextElementSibling.classList.contains('errSpan')) {
                this.input.classList.remove('errInput');
                this.input.nextElementSibling.remove();
            }
            this.input.classList.add('errInput');
            this.input.after(errorSpan);
        }
        this.input.addEventListener('blur', () => {
            if(this.input.value.trim()) {
                this.input.classList.remove('errInput');
                this.input.nextElementSibling.remove();
            }
        });
    }

    event(evt = '', fn) {
        this.input.addEventListener(evt, fn);
    }

    label(text = '', initClass = '') {
        const label = document.createElement('label');
        if(initClass) label.classList.add(initClass);
        label.setAttribute('for', this.input.id);
        label.textContent = text;
        this.input.parentElement.insertBefore(label, this.input);
    }

    get value() {
        return this.input.value;
    }

    set value(newValue) {
        this.input.value = newValue;
    }
}

class TextArea {
    constructor() {
        this.textarea = document.createElement('textarea');
    }

    create() {
        return this.textarea;
    }


    baseAttr(id = '', value = '', placeholder = '', required = '') {
        if(id) this.textarea.setAttribute('id', id);
        if(value) this.textarea.setAttribute('value', value);
        if(placeholder) this.textarea.setAttribute('placeholder', placeholder);
        if(required) this.textarea.setAttribute('required', required);
    }

    label(text = '', initClass = '') {
        const label = document.createElement('label');
        if(initClass) label.classList.add(initClass);
        label.setAttribute('for', this.textarea.id);
        label.textContent = text;
        this.textarea.parentElement.insertBefore(label, this.textarea);
    }

    get value() {
        return this.textarea.value;
    }

    set value(newValue) {
        this.textarea.value = newValue;
    }
}

class Select {
    constructor() {
        this.select = document.createElement('select');
    }

    create() {
        return this.select;
    }

    addOption(text = '', value = '') {
        const option = document.createElement('option');
        if(text) option.textContent = text;
        if(value) option.setAttribute('value', value);
        this.select.append(option);
        return option;
    }

    baseAttr(id = '', disabled = false) {
        if(id) this.select.setAttribute('id', id);
        if(disabled) this.select.setAttribute('disabled', disabled);
    }

    label(text = '') {
        const label = document.createElement('label');
        label.setAttribute('for', this.select.id);
        label.textContent = text;
        this.select.parentElement.insertBefore(label, this.select);
    }

    get value() {
        return this.select.value;
    }

    set value(newValue) {
        this.select.value = newValue;
    }
}
// клас визит с рабочим вариантом моделки можешь
class Visit {
    //фильтри
    static visits = [];
    constructor() {
        this.visitModal = new Modal();
        this.visitForm = new Form();

       //фильр
        Visit.visits.push(this);


        this.visitModal.title('Заполните карту пациента', 'visitModal-title');

        this.visitReason = new Input();
        this.visitReason.baseAttr('text', 'reasonField', '', 'Пример: Сильная боль', 'required');

        this.visitIntro = new TextArea();
        this.visitIntro.baseAttr('introField', '', 'Опишите проблему...', 'required');

        this.visitEmergency = new Select();
        this.visitEmergency.addOption('Обычная', 'Обычная');
        this.visitEmergency.addOption('Приоритетная', 'Приоритетная');
        this.visitEmergency.addOption('Неотложная', 'Неотложная');

        this.visitFullName = new Input();
        this.visitFullName.baseAttr('text', 'fullNameField', '', 'Пример: Иванов Иван Иванович', 'required');

        this.visitSubmit = new Input();
        this.visitSubmit.baseAttr('submit', 'visitSubmit', 'Создать карту');
    }
}

class VisitDentist extends Visit {
    constructor() {
        super();
    }

    create() {
        this.visitModal.create();
        this.visitModal.insert(this.visitForm.create());
        this.visitForm.insert(
            this.visitReason.create(),
            this.visitIntro.create(),
            this.visitEmergency.create(),
            this.visitFullName.create(),
            this.visitSubmit.create(),
        );
        this.visitReason.label('Цель визита', 'dent-label');
        this.visitIntro.label('Краткое описание', 'dent-label');
        this.visitEmergency.label('Срочность', 'dent-label');
        this.visitFullName.label('ФИО пациента', 'dent-label');

        return this.visitModal;
    }

    event(evt = '', fn) {
        this.visitSubmit.event(evt, fn);
    }

    close() {
        this.visitModal.close()
    }

    setValues(newReason = 'Пусто', newIntro = 'Пусто', newEmergency = 'Пусто', newFullName = 'Пусто', newSubmit = 'Сохранить изменения') {
        this.visitReason.value = newReason;
        this.visitIntro.value = newIntro;
        this.visitEmergency.value = newEmergency;
        this.visitFullName.value = newFullName;
        this.visitSubmit.value = newSubmit;
    }

    get value() {
        return {
            'Врач': 'Стоматолог',
            'Цель': this.visitReason.value,
            'Краткое описание': this.visitIntro.value,
            'Срочность': this.visitEmergency.value,
            'ФИО': this.visitFullName.value,
        };
    }

    set title(newTitle) {
        return this.visitTitle.textContent = newTitle;
    }
}

class VisitTherapist extends Visit {
    constructor() {
        super();
        this.visitAge = new Input();
        this.visitAge.baseAttr('text', 'ageField', '', 'Пример: 18', 'required');
    }

    create() {
        this.visitModal.create();
        this.visitModal.insert(this.visitForm.create());
        this.visitForm.insert(
            this.visitReason.create(),
            this.visitIntro.create(),
            this.visitEmergency.create(),
            this.visitFullName.create(),
            this.visitAge.create(),
            this.visitSubmit.create(),
        );
        this.visitReason.label('Цель визита', 'ther-label');
        this.visitIntro.label('Краткое описание', 'ther-label');
        this.visitEmergency.label('Срочность', 'ther-label');
        this.visitFullName.label('ФИО пациента', 'ther-label');
        this.visitAge.label('Возраст пациента', 'ther-label')

        return this.visitModal;
    }

    error() {
        if(!this.visitReason.value.trim() || !this.visitFullName.value.trim() || !this.visitAge.value.trim()) {
            this.visitReason.error();
            this.visitFullName.error();
            this.visitAge.error();
            return;
        }
    }

    event(evt = '', fn) {
        this.visitSubmit.event(evt, fn);
    }

    close() {
        this.visitModal.close();
    }

    setValues(newReason = 'Пусто', newIntro = 'Пусто', newEmergency = 'Пусто', newFullName = 'Пусто', newAge = 'Пусто', newSubmit = 'Сохранить изменения') {
        this.visitReason.value = newReason;
        this.visitIntro.value = newIntro;
        this.visitEmergency.value = newEmergency;
        this.visitFullName.value = newFullName;
        this.visitAge.value = newAge;
        this.visitSubmit.value = newSubmit;
    }

    get value() {
        return {
            'Врач': 'Терапевт',
            'Цель': this.visitReason.value,
            'Краткое описание': this.visitIntro.value,
            'Срочность': this.visitEmergency.value,
            'ФИО': this.visitFullName.value,
            'Возраст': this.visitAge.value,
        };
    }
}

class VisitCardiologist extends Visit {
    constructor() {
        super();
        this.visitPressure = new Input();
        this.visitBMI = new Input();
        this.visitPrevDiseases = new Input();
        this.visitAge = new Input();

        this.visitPressure.baseAttr('text', 'pressureField', '', 'Пример: 120/80', 'required');
        this.visitBMI.baseAttr('text', 'BMIField', '', 'Пример: 8.5', 'required');
        this.visitPrevDiseases.baseAttr('text', 'prevDiseasesField', '', 'Пример: Инфаркт', 'required');
        this.visitAge.baseAttr('text', 'ageField', '', 'Пример: 18', 'required');
    }

    create() {
        this.visitModal.create();
        this.visitModal.insert(this.visitForm.create());
        this.visitForm.insert(
            this.visitReason.create(),
            this.visitIntro.create(),
            this.visitEmergency.create(),
            this.visitFullName.create(),
            this.visitPressure.create(),
            this.visitBMI.create(),
            this.visitPrevDiseases.create(),
            this.visitAge.create(),
            this.visitSubmit.create(),
        );
        this.visitReason.label('Цель визита', 'cardio-label');
        this.visitIntro.label('Краткое описание', 'cardio-label');
        this.visitEmergency.label('Срочность', 'cardio-label');
        this.visitFullName.label('ФИО пациента', 'cardio-label');
        this.visitPressure.label('Обычное давление пациента', 'cardio-label');
        this.visitBMI.label('Индекс массы тела пациента', 'cardio-label');
        this.visitPrevDiseases.label('Перенесённые болезни сердца', 'cardio-label');
        this.visitAge.label('Возраст пациента', 'cardio-label');

        return this.visitModal;
    }

    event(evt = '', fn) {
        this.visitSubmit.event(evt, fn);
    }

    close() {
        this.visitModal.close();
    }

    setValues(newReason = 'Пусто', newIntro = 'Пусто', newEmergency = 'Пусто', newFullName = 'Пусто', newPressure = 'Пусто', newBMI = 'Пусто', newPrevDiseases = 'Пусто', newAge = 'Пусто', newSubmit = 'Сохранить изменения') {
        this.visitReason.value = newReason;
        this.visitIntro.value = newIntro;
        this.visitEmergency.value = newEmergency;
        this.visitFullName.value = newFullName;
        this.visitPressure.value = newPressure;
        this.visitBMI.value = newBMI;
        this.visitPrevDiseases.value = newPrevDiseases;
        this.visitAge.value = newAge;
        this.visitSubmit.value = newSubmit;
    }

    get value() {
        return {
            'Врач': 'Кардиолог',
            'Цель': this.visitReason.value,
            'Краткое описание': this.visitIntro.value,
            'Срочность': this.visitEmergency.value,
            'ФИО': this.visitFullName.value,
            'Обычное давление': this.visitPressure.value,
            'ИМТ': this.visitBMI.value,
            'Перенесенныё болезни ССС': this.visitPrevDiseases.value,
            'Возраст': this.visitAge.value,
        };
    }

    set title(newTitle) {
        return this.visitTitle.textContent = newTitle;
    }
}




export {
    Modal,
    Form,
    Input,
    TextArea,
    Select,
    Visit,
    VisitDentist,
    VisitTherapist,
    VisitCardiologist
}