

const loginButton = document.getElementById('loginButton');
const createVisitButton = document.getElementById('createVisitButton');
const defaultText = document.querySelector('.visit__field__default-text');
const visitField = document.querySelector('.visit__field__inner');
const loginURL = 'https://ajax.test-danit.com/api/cards/login';
const generalUrl = 'https://ajax.test-danit.com/api/cards';

export {
    loginButton,
    createVisitButton,
    defaultText,
    visitField,
    loginURL,
    generalUrl,
}